import log from '@ajar/marker'
import net from 'net'

const { port } = process.env;

const socket = net.createConnection({ port }, _ => {
   log.yellow('✨ ⚡connected to server! ⚡✨');
   socket.write('{"search_word":"messi"}');
});

socket.on('data', buffer => {
    log.v('-> value:',buffer.toString());
});
  
socket.on('end', () => { 
  log.yellow('✨ ⚡disconnected from server ⚡✨');
});