import log from '@ajar/marker'
import net from 'net'
import Twitter from 'twitter'
import Sentiment from 'sentiment'
const { port } = process.env;
const server = net.createServer();
const { consumer_key, consumer_secret, access_token_key, access_token_secret } = process.env;
const credentials = { consumer_key, consumer_secret, access_token_key, access_token_secret }
const client = new Twitter(credentials);
const sentiment = new Sentiment();
let twitCount = 0;
let totalSentiments = 0;
let highest = 0;
let lowest = 0;
let my_query = "";

server.on('connection', socket => {
  log.yellow('✨ client connected ✨');

  socket.on('data', buffer => {
    log.blue('-> incoming:', buffer.toString().trim());

    my_query = JSON.parse(buffer.toString()).search_word;
    const t = client.stream('statuses/filter', { track: my_query });

    t.on('data', twit => {
      if (twit.lang == 'en') {

        const { score } = sentiment.analyze(twit.text);
        twitCount++;
        totalSentiments += score;

        if (score > highest) highest = score;
        if (score < lowest) lowest = score;

        const avg = totalSentiments / twitCount;
        socket.write(`The twit below:⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣⇣\n${twit.text} \n has sentiment score of :${score}
        \n ╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍╍`);
      }

    });
    t.on('error', error => {
      log.error(error);
    });

    socket.on('end', () => {
      log.yellow('✨ client disconnected ✨');
    });
  });
  socket.on('error', error => {
    log.error(error);
  });
  server.on('end', () => {
    log.yellow('✨ server disconnected ✨');
  });
});

server.on('error', err => log.error(err));
server.listen(port, () => log.v('✨ ⚡Server is up  🚀'));




